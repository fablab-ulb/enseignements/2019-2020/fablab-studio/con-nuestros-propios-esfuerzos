# Bain de bouche à l'origan - 101 / 4

> traduit par : Sara Hansali

| Colutorios de orégano | Bain de bouche à l'origan |
| --- | --- |
| <ul><li> Extracto fluído de orégano : 20 mL </li><li> Alcohol (40%) : 40 mL | <ul><li> Extrait fluide d'origan : 20 ml </li><li> Alcool (40%) : 40 ml |
Uso : Alveolitis, gingivitis <br> Dosis : Uso topico, 3 veces al día | Utilisation : Alvéolite, gingivite <br> Dose : Usage local, 3 fois par jour | 
Autor : (SM) Reynaldo González Bosch <br> ISMM : Dr. Luis Diaz Soto, C. Habana. | Auteur : (SM) Reynaldo González Bosch <br> ISMM : Dr. Luis Diaz Soto, C. La Havane |
