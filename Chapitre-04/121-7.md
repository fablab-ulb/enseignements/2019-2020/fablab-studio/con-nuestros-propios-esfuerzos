# Stérilisation de matériau médical - 121 / 7

> traduit par : Abdellatif Siari

|Esterilización del material médico|Stérilisation de matériau médical|
| --- | --- |
| La esterilización del material médico dos veces a la semana como, norma, genera un ahorro considerable en kW/hora, pues como se conoce, anteriormente se hacía hasta 3 veces diarias en consultorios y , policlínicos.<br><br>Haciéndolo como inicialmente se explica, 2 veces a la semana; agrupando el material a procesar, conlleva a una disminución considerable en el ahorro de energía eléctrica.|La stérilisation du matériel médical deux fois par semaine génère généralement des économies considérables en kW / heure car, comme on le sait, elle était pratiquée jusqu’à trois fois par jour dans les cliniques et les polycliniques.<br><br>Le faire comme expliqué initialement, deux fois par semaine; groupement de la matière à traiter conduit à une réduction considérable de l’économie d'énergie électrique.|
|Autores: Colectivo de enfermería.<br>Policlinico “Dr. Diego Tamayo”.<br>Habana Vieja, Ciudad Habana.| Auteurs: Collectif infirmier.<br>Polyclinique “Dr. Diego Tamayo”.<br>Vieille Havane, Ville de La Havane.|