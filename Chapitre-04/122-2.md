# Déplasmatisation - 122 / 2

> traduit par : Florian Aliker

| Desplasmatizador| Déplasmatisation |
| --- | --- |
| Aprovechar una unidad de refrigerador defectuosa, que logre el vacío suficíente para la extracción del plasma procedente de un frasco de sangre total, mediante la utilización de un frasco receptor, un frasco generador de vacío y 2 equipos extractores de sangre. |Tirez parti d'un réfrigérateur défectueux, qui crée un vide suffisant pour l'extraction du plasma d'une bouteille de sang total, en utilisant un flacon récepteur, un flacon générateur de vide et 2 appareils de collecte de sang.|
| Autores : Dr. Orestes Casañola Romeo. Lic. DeiCina Martínez Cisneros. <br>Jagüey Grande, Matanzas | Auteurs : Dr. Orestes Casañola Romeo. Delfina Martínez Cisneros. <br> Jagüey Grande, Matanzas |
|![122-2.jpg](https://gitlab.com/fablab-ulb/enseignements/2019-2020/fablab-studio/dfs1-atelier/raw/master/Traduction/img/Chapitre%204/122-2.jpg)|![122-2fr.jpg](https://gitlab.com/fablab-ulb/enseignements/2019-2020/fablab-studio/dfs1-atelier/raw/master/Traduction/img/Chapitre%204/122-2fr.jpg)|
