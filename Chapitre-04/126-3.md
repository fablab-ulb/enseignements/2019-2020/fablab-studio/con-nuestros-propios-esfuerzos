# Conversion de berceau horizontal en Fowler - 126 / 3

> traduit par : Florian Aliker

| Conversión de cunas horizontales en Fowler | Conversion de berceau horizontal en Fowler* |
| --- | --- |
| Las cunas en existencia, eran con bastidores rectos, lo que impedía la adecuada posición que el recién nacido, en determinado momento requería. Se le dio un corte al bastidor, para que hiciera las veces de bisagra y de esta forma, permite entonces subir y bajar la cuna. Se le agregó un aditamento muy parecido al de las cunas yugoslavas, para darle diferentes niveles de altura a las camitas. | <p>Les lits d'enfant existants étaient dotés de cadres droits, ce qui empêchait le nouveau-né de bien se positionner à un moment donné. Le cadre a été coupé afin de pouvoir agir comme une charnière et ainsi permettre au berceau d'être élevé et abaissé. Un accessoire très similaire à celui des lits d'enfants yougoslaves a été ajouté, afin de donner différents niveaux de hauteur aux lits. <p>*Le but est de pouvoir installer le nourisson en position de Fowler.|
| Autóres: Jorge Huici Cubas. Santiago Valdés Gonzalez. Armando Diaz Niebla.<br>Cienfuegos, Cienfuegos. | Auteurs: Jorge Huici Cubas. Santiago Valdés Gonzalez. Armando Diaz Niebla.<br>Cienfuegos, Cienfuegos. |
|![Image](https://gitlab.com/fablab-ulb/enseignements/2019-2020/fablab-studio/dfs1-atelier/raw/master/Traduction/img/Chapitre%204/126-3.jpg)|