# Teinture-mère de Belladone - 112 / 6

> traduit par : Florian Aliker

| Belladona tintura | Teinture-mère de Belladone  |
| --- | --- |
|Extracto fluido de belladona ... 10 ml<br>Alcohol (70 %) ... 100 ml<br>Uso : Antiespasmodico de la musculatora lisa.<br>Dosis: 10 goticas cada 4 horas. | Extrait fluide de Belladonna (Atropa belladonna) ... 10 ml<br>Alcool (70%) ... 100 ml<br>Utilisation: antispasmodique des muscles lisses.<br>Dose: 10 gouttes toutes les 4 heures.|
| Santa Cruz del Norte, provincia La Habana. | Santa Cruz del Norte, province de La Habana. |
