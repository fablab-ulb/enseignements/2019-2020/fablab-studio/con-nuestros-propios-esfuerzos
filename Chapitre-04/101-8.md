# Pinosol - 101 / 8

> traduit par : Hansali Sara

| Pinosol | Pinosol |
| --- | --- |
| <ul><li> Extracto fluido de pino macho : 40 mL </li><li> Alcohol (40 %) : 80 ml | <ul><li> Extrait fluide de pin caraïbe : 40 mL </li><li> Alcool (40%) : 80 ml |
|  Técnica : Mezclar el extracto fluido en el alcohol. <br> Uso : Intertrigo, pitiriasis, micosis. <br> Dosis : 3 veces al dia. | Technique : Mélanger l'extrait fluide avec l'alcool. <br> Utilisation: Intertrigo (inflammation des plis cutanée), pityriasis (dermatose), mycose. <br> Dose : 3 fois par jour. |
Autores :(SM) Reynaldo González Bosch <br> (SM) Rodolfo Arencibia. <br> ISMM : Dr. Luis Díaz Soto, Ciudad de la Habana. | Auteurs : (SM) Reynaldo González Bosch <br> (SM) Rodolfo Arencibia. <br> ISMM : Dr. Luis Díaz Soto, Ville de La Havane. |