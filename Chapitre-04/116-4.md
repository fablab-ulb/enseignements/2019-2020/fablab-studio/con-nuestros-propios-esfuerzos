# Moxas - 116 / 4

> traduit par : Abdellatif Siari

|Moxas|Moxas|
| --- | --- |
|Se utilizan hojas secas de plantas medicinales (Artemisa, Incienso o Alcanfor). Después de triturarlas hasta convertirlas en un polvo grueso, se enrollan en forma de tabaco, con cualquier tipo de papel. Actúa como fuente de calor.<br><br>Usos: Siguiendo los' principios de la acupuntura, para aplicar calor en los meridianos de enfermedades originadas por el frio, el viento y estados vacios de energia.|Les feuilles sèches de plantes médicinales (Artemis, Encens ou Camphre) sont utilisées. Après les avoir broyés en une poudre grossière, ils sont roulés sous forme de tabac, avec n'importe quel type de papier. Il agit comme une source de chaleur.<br><br>Utilisations: Selon les principes de l'acupuncture, appliquer de la chaleur sur les méridiens des maladies causées par le froid, le vent et des états énergétiques vides.|
|San Cristobal, Pinar del Rio.|San Cristobal, Pinar del Rio.|
