# Confection de cônes de Gutta-percha (Stomatologie)- 126 / 2

> traduit par : Florian Aliker

| Confección de conos de Gutaperchas (Estomatologia) | Confection de cônes de Gutta-percha (Stomatologie) |
| --- | --- |
|Los conos de gutaperchas para obturación de los conductos rádiculares, se están realizando de forma manual, aportando un ahorro considerable de divisas al pais en la clinica estomatológica de especialidades (servicio de endodoncia).|<p>Les cônes de gutta-percha* destinés à remplir les canaux radiculaires sont réalisés manuellement, ce qui permet au pays de réaliser des économies financières considérables pour la clinique spécialisée en stomatologique (service d’endodontie).<p>*gomme issue du latex naturel |
| Autor : Sobelda Quezada.<br> Cienfuegos, Cienfuegos. | Auteur : Sobelda Quezada.<br>Cienfuegos, Cienfuegos. |