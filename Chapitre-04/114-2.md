# Crème de boue - 114 / 2

> traduit par : Abdellatif Siari

|Crema de fango| Crème de boue|
| --- | --- |
|Vaselina .. 140g<br>Lanolina .. 4g<br>Polvo de fango  .. 100g<br>Agua sulfurada .. 260ml<br>Aceite ricino .. 100ml<br><br>Usos: Fonoforeses (ultrasonido) y estados inflamatorios de articulaciones.<br>Sustituye la hidrocortizona.| Vaseline .. 140g<br>Lanoline .. 4g<br>Poudre de boue .. 100g<br>Eau de soufre .. 260ml<br>Huile de ricin .. 100ml<br><br>Utilisations: Phonophorèses (ultrasons) et états inflammatoires des articulations.<br>Il remplace hydrocortizona.
|Autors: José A. Soto, Luis Rodriguez, Mercedes ViIlalón et Marisol Gamez.<br> Los Palacios, Pinar del Rio.| Auteurs: José A. Soto, Luis Rodriguez, Mercedes ViIlalón et Marisol Gamez.<br>Les palais, Pinar del Rio.|