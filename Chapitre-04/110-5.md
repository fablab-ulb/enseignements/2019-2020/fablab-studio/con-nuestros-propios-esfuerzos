# Miel stimulant - 110 / 5

> traduit par : Florian Aliker

| Mielitus estimulante | Miel stimulant |
| --- | --- |
| Cola extracto fluido ... 200 ml<br>Jengibre tintura (50 %) ... 20 ml<br>Naranja dulce (aceite esencial) ... 0,5 ml<br>Glicerina ... 40 ml <br>Metilparabeno ... 2,4 g<br>Propilparabeno ... 0,4 g<br>Alcohol etílico ... 10 ml<br>Miel de abeja o jarabe ... 1000 ml<br>Uso : Estimulante.<br>Dosis : 3 cucharaditas al día.| Extrait de cola ... 200 ml<br>Teinture-mère de gingembre (50%) ... 20 ml<br>Orange douce (huile essentielle) ... 0,5 ml<br>Glycérine ... 40 ml<br>Methylparaben ... 2,4 g<br>Propylparaben ... 0,4 g<br>Alcool éthylique ... 10 ml<br>Miel d'abeille ou sirop ... 1000 ml<br>Utilisation : stimulant.<br>Dose: 3 cuillères à café par jour. |
| Santa Cruz del Norte, provincia La Habana | Santa Cruz del Norte, provincia La Habana |
