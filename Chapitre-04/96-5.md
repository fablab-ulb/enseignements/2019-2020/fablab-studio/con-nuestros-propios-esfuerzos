# Traitement du psoriasis - 96 / 5

> traduit par : Sara Hansali

| Tratamiento de soriasis | Traitement du psoriasis |
| --- | --- |
| Someter a los pacientes al tratamiento con fangos y aguas medicinales, y exposición al sol por períodos determinados. <br> La experiencia se realizó con 42 pacientes aquejados del soriasis (en distintos grados). El tratamiento duró 30 días. |Soumettez les patients à un traitement avec de la boue et des eaux médicinales, et à une exposition au soleil pendant certaines périodes. <br> L'expérience a été réalisée chez 42 patients atteints de psoriasis (de grades différents). Le traitement a duré 30 jours. |
Resultando : <ul><li> 30 pacientes se curaron (71 %). </li><li> 10 pacientes mejoraron (24 %). </li><li> 2 pacientes se detuvo el desarrollo de la enfermedad (5 %) | Résultats : <ul><li> 30 patients ont été complètement guéris (71%). </li><li> 10 patients ont vu leur état s'améliorer (24%). </li><li> 2 patients ont vu l'arrêt du développement de la maladie (5%)
|Grupo Nacional de Piel, Balneario Elguea. <br> Corralillo, Villa Clara.| Groupe national de la peau, Thermes d'Elguea <br> Corralillo, Villa Clara. |