# Gouttes pour ulcères - 101 / 7

> traduit par : Sara Hansali

| Gotas para ulceras | Gouttes pour ulcères |
| --- | --- |
| <ul><li>Tintura de romerillo (20%) : 60 mL </li><li> Tintura de menta americana (20%) : 60 mL | <ul><li> Teinture de bidents (20%) : 60 mL </li><li> Teinture de menthe poivrée (20%) : 60 ml |
Uso : Gastritis, ulcera deudenal. <br> Dosis : 10 gotas, 15 minutos antes del desayuno, almuerzo y comida. Si hay dolor, epigastragia se pueden ingerir hasta 20 gotas. Siempre se deben tomar disueltas en agua.| Utilisation : Gastrite, ulcère deudenal. <br> Dose : 10 gouttes, 15 minutes avant le petit-déjeuner, le déjeuner et le diner. En cas de douleur ou d'épigastragie, la dose peut être augmentée jusqu'à 20 gouttes. Elles doivent toujours être dissoutes dans l'eau.
| Autor : (SM) Reynaldo Gonzàlez Bosch <br> ISMM : Dr. Luis Diaz Soto, Ciudad de la Habana | Auteur : (SM) Reynaldo Gonzàlez Bosch <br> ISMM : Dr. Luis Diaz Soto, Ville de La Havane |



