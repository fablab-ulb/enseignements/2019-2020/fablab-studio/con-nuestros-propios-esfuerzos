# Hydrojamil - 98 / 7

> traduit par : Sara Hansali

| Hydrojamil | Hydrojamil |
| --- | --- |
| <ul><li>Ungüento hidrófilo : 120 g </li><li>Miel : 10 g </li><li> Jalea Real : 2 g |<ul><li>Pommade hydrophile : 120 g </li><li> Miel : 10 g </li><li> Gelée royale : 2 gr |
Usos : Quemaduras y cosméticos| Utilisations : Brûlures et cosmétiques
| Autores : José A. Soto, Lino Rodríguez, Mercedes Villalón y Marisol Gómez <br> Los Palacios, Pinar del Río | Auteurs : José A. Soto, Lino Rodríguez, Mercedes Villalón et Marisol Gómez <br> Les Palacios, Pinar del Río  |