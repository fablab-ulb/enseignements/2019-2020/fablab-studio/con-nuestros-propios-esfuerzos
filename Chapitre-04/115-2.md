# Equipement manuel pour la traction cervicale - 115 / 2

> traduit par : Abdellatif Siari

|Equipo manual para la tracción cervical|Equipement manuel pour la traction cervicale|
| --- | --- |
|Fabricar la barra de tracción utilizando un angular y en su parte horizontal de 60 cm de largo soldar 2 rondanas. Después, atar a uno de los extremos de la cuerda que pasa por ambas la Fronda de Glisón y en el otro el peso para la tracción según el peso corporal del paciente. La Fronda de Glisón es construida con materiales de recorteria.|Fabriquer le timon en utilisant un angle et dans sa partie horizontale, souder 2 rondelles de 60 cm de long. Ensuite, attachez à une extrémité de la corde qui traverse la Glisón Frond et à l’autre le poids de traction en fonction du poids du patient. La Fronde de Glisón est construite avec des matériaux découpés.|
|Autores: Rafael Gimenez, Amado Dominguez / Félix Giminez.<br>Urbano Noris, Holguin.|Auteurs: Rafael Gimenez, Amado Dominguez / Félix Giminez.<br>Noris urbain, Holguin.|