# Mélange alimentaire créoles destinés aux volailles en période de reproduction et de ponte - 37 / 2

> traduit par : Banga Fatima

| Pienso criollo para aves ponedoras y reproductoras | Mélange alimentaire créoles destinés aux volailles en période de reproduction et de ponte |
| --- | --- |
| <strong>Formula : | <strong>Recette : |
| <ul><li>Harina de yuca... 15%<li>Harina de millo... 17,6%<li>Harina de carne (desecho)... 2%<li>Harina de langosta (desechos)... 15%<li>Polvo de arroz... 10,1%<li>Azúcar... 25%<li>Carbonato de calcio... 9%<li>Zeolita... 3%<li>Fosfato di cálcico... 3%<li>Sal común... 0,3%<li>Cornogrisin-40... 200g/t | <ul><li>Farine de manioc... 15 %<li>Farine de gofio... 17,6 %<li>Farine de viande (reste)... 2 %<li>Farine de langouste (reste)... 15 %<li>Poudre de riz... 10,1 %<li>Sucre... 25 %<li>Carbonate de calcium... 9 %<li>Zéolithe... 3 %<li>Phosphate de calcium... 3 %<li>Du sel commun... 0,3 %<li>Cornogrisin-40... 200 g/t|
| <span class="underline"><strong>Autores :</span><br></strong> Dr. Juan Luis Méndez y Lic. Luis Rojas Hernández. | <span class="underline"><strong>Auteurs :</span><br></strong> Dr. Juan Luis Méndez et Lic. Luis Rojas Hernández. |
| San Cristóbal, Pinar del Rio.|
