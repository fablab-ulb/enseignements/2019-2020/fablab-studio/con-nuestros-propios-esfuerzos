# Extracteur manuel d'apiculture - 46 / 1
> traduit par : Younes Cassandra

| Extractor manual apícola   | Extracteur manuel d'apiculture |
| --- | --- |
|Consiste en un tanque de chapa galvanizada que lleva en su interior un elemento en el cual se colocan los pañales para la extracción de la miel. Esto gira a gran velocidad mediante un mecanismo de transmisión. <br> Conformado con: <ul><li>  Chapa. </li><li> Piezas de fundición maquinadas. </li><li> Estructuras soldadas.</p><br> La energía que emplea para su funcionamiento es la fuerza del hombre.  |Il consiste en un réservoir en tôle galvanisée qui contient un élément dans lequel sont placées des couches pour extraire le miel. Il tourne à grande vitesse à travers un mécanisme de transmission. <br> Conforme à: <ul><li> Tôle. </li><li> Pièces moulées. </li><li> Structures soudées.</p> <br>L'énergie utilisée pour son fonctionnement est la force de l'homme. |
|  Plaza de la Revolución, Ciudad de La Habana    | Place de la révolution, Ville de la Havane  |||
