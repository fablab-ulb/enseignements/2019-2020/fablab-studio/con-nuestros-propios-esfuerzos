# Enfumoir - 46 / 2
> traduit par : Younes Cassandra

| Ahumador apícola    | Enfumoir |
| --- | --- |
|Se conforma mediante moldes o troqueles utilizando para ello chapas galvanizadas. Se emplea en la atención a las abejas, así como en las labores de extracción de miel. |Il est façonné par des moules ou des matrices à l'aide de tôles galvanisées. Il est utilisé dans l'attention des abeilles, ainsi que pour les travaux d'extraction du miel.|
|  Plaza de la Revolución, Ciudad de La Habana    | Place de la révolution, Ville de la Havane  |||
