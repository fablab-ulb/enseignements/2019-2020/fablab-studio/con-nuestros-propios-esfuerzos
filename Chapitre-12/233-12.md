# Fibre Doyle.- 233 / 12

> traduit par : Julien Zabala

|Doyle de fibra. |Fibre Doyle.|
| --- | --- |
Mantener la fibra en proceso de secado 15 días; después recortarla y lavarla. Tejer en las maquinas (telares). Trazar el objeto y coser por encima del trazo. Recortar según este. Cortar los vivos e la tela y coserlos. |Gardez la fibre dans le processus de séchage pendant 15 jours; puis coupez-le et lavez-le. Tricoter sur les machines. Tracez l'objet et cousez sur le trait. Coupez selon cela. Coupez le tissu et le tissu et cousez-les.
Cumanayagua, Cienfuegos. |Cumanayagua, Cienfuegos.