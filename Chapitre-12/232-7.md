# Pantoufles en fibres. - 232 / 7

> traduit par : Julien Zabala

|Pantuflas confeccionadas con fibra. |Pantoufles en fibres.|
| --- | --- |
Este es un tipo de calzado destinado a usar en el hogar. Para su confección hacer la plantilla; coserla encima de la tela y después sobre el tejido de fibra; luego recortar por el borde de la plantilla. |Ceci est un type de chaussures destinées à un usage domestique. Pour sa préparation faire le gabarit; cousez-le sur le tissu puis sur le tissu en fibres; Ensuite, coupez le long du bord du gabarit.
Para la parte de abajo hacer la plantilla del numero deseado, coser encima de la tela y después de la fibra. Recortar por el borde de la plantilla; unir la parte de arriba y la de abajo con la maquina. Después rematar por todo el borde con la fibra preparada para lo cual es necesario abrirla a todo lo largo y luego sacarle lo de adentro. Finalmente coser de forma manual la suela de noelita|Pour la pièce ci-dessous, confectionnez le gabarit du nombre souhaité, cousez-le sur le tissu et après la fibre. Coupez le long du bord du gabarit; rejoindre le haut et le bas avec la machine. Ensuite, terminez tout le bord avec la fibre préparée pour laquelle il est nécessaire de l'ouvrir tout le temps, puis sortez-le à l'intérieur. Enfin, coudre manuellement la semelle de noelite.|
Puerto Padre, Las Tunas.|Puerto Padre, Las Tunas.|