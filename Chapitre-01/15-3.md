# Production d'enzymes, de dextranza et de biopreparations - 15 / 3

> traduit par : Hélène Menu

| Produccion de enzimas, dextranaza y biopreparados  | Production d'enzymes, de dextranza et de biopreparations |
| --- | --- |
| Es una tecnologia industrial a partir de procesos fermentativos por cultivo y desarrollo de micro-organismos, hajo determinadas condiciones climaticas y asepticas, partiendo de cepas elaboradas en ICINAZ e INIFA. | Il s’agit d’une technologie industrielle reposant sur des processus de fermentation par culture et développement de micro-organismes, dans certaines conditions climatiques et aseptiques, à partir de souches fabriquées à ICINAZ et à INIFA. |
| La dextranaza es empleada para mejorar el proceso tecnologico en la produccion de azucar crudo. Promueve y desarrolla la dextrana porque elimina un gran por ciento de la gamas indeseables en las masas y por ello, disminuye su viscosidad, facilitando la fluidez. |Dextranza est utilisé pour améliorer le processus technologique dans la production de sucre brut. Favorise et développe le dextran, car il élimine un grand pourcentage de plages indésirables dans les masses et diminue donc sa viscosité, facilitant ainsi la fluidité. |
|El azotobacter puede emplearse en sustitucion de los fertizantes notrogenados, debido a que su applicationn provoca el aumento de nitrogeno del suelo necesario para una mayor germinacion de la semilla, asi como para su crecimiento en un periodo breve. |Azotobacter peut être utilisé comme substitut des engrais notrogénés, car son application provoque l'augmentation de l'azote du sol nécessaire à une plus grande germination de la graine, ainsi qu'à sa croissance dans une courte période.|
|Su fincion es producir sustancias fisiologicamente activas y fijacion del nitrogeno atmosferico a la planta. |Son but est de produire des substances physiologiquement actives et de fixer l'azote atmosphérique à la plante.|
