# Tracteur multi-usage - 22 / 2

> traduit par : Katia Mot

| Cultivadora de tractor para uso multiple | Tracteur multi-usage
| --- | --- |
| Es un implemento resistente capaz de realizar distintas labores en la agricultura. Sirve para cultivar platano, yuca, boniato, frijoles, maiz, tomate, aji, calabaza. Lo mismo cultiva que surca prepara la tierra, cruza o  da grada. | Il s’agit d’un outil résistant capable d’exécuter différentes tâches en agriculture. Il est utilisé pour la culture de bananes, manioc, patates douces, haricots, maïs, tomates, poivrons, courges. La même cultive que sillon prépare la terre, traverse ou se dresse. |
| Segun el trabajo cambia sus organos; tiene un ancho de labor de 2,5 metros y puede profundizar hasta 12 pulgadas. En une jornada logra cultivar aporcar o dar grada a una caballeria: en roturacion solo logra la mitad. | Selon le travail change ses organes; Il a une largeur de 2,5 mètres et peut s’approfondir jusqu’à 12 pouces. En un jour, il parvient à cultiver un cheval ou une niveleuse une cavalerie: en cas de casse, il ne réalise que la moitié. |
Gibara, Holguin