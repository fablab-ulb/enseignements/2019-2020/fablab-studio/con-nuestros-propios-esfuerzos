# Telera pour charrue créole - 31 / 4

> traduit par : Katia Mot

| Telera para arado criollo | Telera pour charrue créole |
| --- | --- |
A una cabilla de 20 pulgadas de largo por 20 centimetros de diametro, se Ie hace rosca y tuerca con dos soportes para el ajuste. | Dans une cabine de 20 pouces de long et de 20 centimètres de diamètre, le filetage et l’écrou sont fabriqués avec deux supports de réglage. |
| San Juan y Martinez, Pinar del Rio |
| Telera = Pièce utilisée pour renforcer l’union de deux autres, afin que l’ensemble reste indéformable. Agriculture: Traverse qui fixe le dentaire au lit ou au gouvernail de charrue