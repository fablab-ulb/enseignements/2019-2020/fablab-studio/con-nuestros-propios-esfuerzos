# Vérification du bord des pales du KTP-1 - 30 / 1

> traduit par : Hélène Menu

|Comprobacion del filo a las cuchillas de la KTP-1 |Vérification du bord des pales du KTP-1 |
| --- | --- |
|La estampa inferior esta conformada por una planchuela de 20 milimetros, a la cual le suedan dos pedazos de cuchillas del centro de acopio. La estampa superior es un quadrado de acero de 60 por 60 sujeto al piston de la prensa. |L’empreinte inférieure est composée d’une plaque de 20 millimètres qui ressemble à deuxmorceaux de lames du centre de collecte. La partie supérieure est un carré en acier de 60 carrés60 attaché au piston de la presse.
|Para el conformado, se calienta al rojo vivo en la fragua, uno de los extremos del segmento de corte. Seguidamente, debe ser introducido una fuerza de 30 toneladas, con lo cual son hechos los dos filos del degmento. Este metodo presenta una alta productividad y ahorra piedra de esmeril.|Pour la formation, le rouge chaud est chauffé dans la forge, une des extrémités du segment de coupe.Ensuite, il faut introduire une force de 30 tonnes, avec laquelle les deux bords sont faitsde la dégustation Cette méthode a une productivité élevée et économise la pierre d'émeri.

