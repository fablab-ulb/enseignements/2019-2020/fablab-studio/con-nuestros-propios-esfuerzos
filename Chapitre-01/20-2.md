# Séchoirs à café transformés en vergers - 20 / 2

> traduit par : Katia Mot

| Secadores de cafe convertidos en huerto | Séchoirs à café transformés en vergers |
| --- | --- |
| Con el objetivo de producir hortalizas idearon la construccion de canteros sobre las ins­talaciones destinadas a secade­ros de cafe, al concluir la zafra. En estos huertos improvisados, los cuales ofrecen resultados positivos, por cada metro cua­drado pueden obtenerse hasta 15 libras de lechuga, al tiempo de brindar proteccion del sol a los patios de secaderos. | Dans le but de produire des légumes, ils ont conçu des plates-bandes sur les installations destinées aux séchoirs à café, à la fin de la récolte. Dans ces vergers improvisés, qui offrent des résultats positifs, on peut obtenir jusqu'à 15 kg de laitue par mètre carré, tout en offrant une protection solaire aux cours de séchage. |
| Para la creacion de estos canteros pueden ser utilizados ladrillos en desuso palos tallos de platano, piedras, etc. Estos canteros, de acuerdo con el tamano del secadero, pueden tener 30 metros de largo por un metro de ancho donde seran depositados una capa de tierra de 15 centimetros de grosor y otra de materia organica de igual espesor. | Pour la création de ces carrières, on peut utiliser des bâtons de briques désaffectés, des tiges de banane, des pierres, etc. Ces carrières, en fonction de la taille du séchoir, peuvent mesurer 30 mètres de long sur un mètre de large sur lequel une couche de terre de 15 centimètres d'épaisseur et une autre matière organique d'épaisseur égale seront déposées. | 
Segundo Frente, Santigo de Cuba
