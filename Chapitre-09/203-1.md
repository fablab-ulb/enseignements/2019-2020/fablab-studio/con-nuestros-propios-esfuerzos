# Methode de reparation de chaudieres - 203 / 1

> traduit par : Joseph Jadam

|Método para la reparación de calderas |Methode de reparation de chaudieres|
| --- | --- |
|Consiste en utilizar el aire de combustión , como elemento principal de diagnóstico , el cual nos permite hallar las averías y realizar la reparación simul táneamente con las pruebas , economizando el tiempo de re paración , así como su costo. |Il consiste à utiliser l'air de combustion comme principal élément de diagnostic, ce qui nous permet de localiser les défauts et d'effectuer la réparation en même temps que les tests, ce qui permet de gagner du temps, ainsi que de réduire les coûts.|
|Santiago de Cuba , Santiago de Cuba.|Santiago de Cuba, Santiago de Cuba.|