# Gasogenerateurs - 203 / 4

> traduit par : Joseph Jadam

|Gasogeneradores |Gasogenerateurs|
| --- | --- |
|A un motor de turbina para regar tabaco ( 8 pulg ) se le adi ciona un gasogenerador , consis tente en dos tanques metálicos : en el más grande se deposita el carbón vegetal y en el más pequeño , se recepcionan los ga ses , que después pasan al puri ficador del motor a través de dos tubos colocados en V , que van a las válvulas. |A une turbomachine pour irriguer le tabac (8 in.) Un générateur de gaz est ajouté, composé de deux réservoirs métalliques: dans le plus grand, le charbon est déposé et dans le plus petit, les gaz sont reçus, qui passent ensuite au purificateur du moteur par deux tubes placés en V qui vont aux soupapes.|
|Ventajas : Disminuye en 35 % el consumo de petróleo ( diesel ) por horas. |Ventajas : Disminuye en 35 % el consumo de petróleo ( diesel ) por horas. |
|San Juan y Martinez , Pinar del Rio.|San Juan y Martinez , Pinar del Rio.|