# Moule pour faire fondre le briseur de lada dans du plastique. - 145 / 6

> traduit par : Julien Zabala

|Molde para fundir en plástico el ruptor del lada. |Moule pour faire fondre le briseur de lada dans du plastique.|
| --- | --- |
|Confeccionar un molde en tres partes, al que se le unen los tres elementos para la inyección del plástico, cuando se va a fundir. Posteriormente proceder al desarme para obtener la pieza fabricada.|Fabriquez un moule en trois parties, auquel seront fixés les trois éléments pour l’injection plastique, lorsqu’il va fondre. Ensuite, procédez au démontage pour obtenir la pièce fabriquée.
Esta pieza se utiliza en la distribución de corriente de los autos LADA y FIAT.|Cette partie est utilisée dans la distribution actuelle des voitures LADA et FIAT.
Finar del Rio, Pinar del Rio.|Finar del Rio, Pinar del Rio.|