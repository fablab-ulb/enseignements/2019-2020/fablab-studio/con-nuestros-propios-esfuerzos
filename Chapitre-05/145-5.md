# Moules pour fabriquer le joint frontal. - 145 / 5

> traduit par : Julien Zabala

|Moldes para fabricar retén delantero.|Moules pour fabriquer le joint frontal.|
| --- | --- |
|Utilizar un molde de metal con las mismas estructuras del retén y fabricar la base, con goma cruda y calor, hacer los retenes en una plancha eléctrica, los cuales evitan los salideros de aceite en el motor y reducen el consumo.|Utilisez un moule en métal avec les mêmes structures que le joint et faites la base, avec le caoutchouc brut et la chaleur, faites les joints sur un fer à repasser électrique, ce qui évite les fuites d'huile dans le moteur et réduit la consommation.
Santo Domingo, Villa Clara.|Santo Domingo, Villa Clara.|
