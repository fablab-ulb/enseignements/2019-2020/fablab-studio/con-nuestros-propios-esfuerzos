# Système de récupération de carburant sur 1 200 remorqueurs - 149 / 3

> traduit par : Julien Zabala

|Sistema de recuperación de combustible en los remolcadores de 1 200 HP|Système de récupération de carburant sur 1 200 remorqueurs|
| --- | --- |
Sistema de recuperación de combustible en los remolcadores de 1 200 HP. Instalar un sistema de tuberías capaz de recolectar el exceso de combustible derramado, por el retorno de los inyectores de la maquina principal, lo cual implica un ahorro, al reutilizarlo.|Système de récupération de carburant sur les 1 200 HP remorqueurs. Installez un système de tuyauterie capable de collecter l'excès de carburant déversé, en raison du retour des injecteurs de la machine principale, ce qui implique des économies, lorsqu'il est réutilisé.
Habana Vieja, Ciudad de La Habana.|Habana Vieja, Ciudad de La Habana.|