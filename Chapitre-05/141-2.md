# Couplage de la transmission intermédiaire du côté. - 141 / 2

> traduit par : Julien Zabala

| Construcción de filtre de aceite para motores hiño.| Couplage de la transmission intermédiaire du côté. |
| --- | --- |
Construir a partir de filtros desechados, los cuales utilizan como elemento filtrante, una malla inoxidable con orificios muy pequeños, por donde sólo pasa el aceite a presión, recha­zando todas las impurezas y limallas. | Construit à partir de filtres mis au rebut, qui utilisent comme élément filtrant, un maillage en acier inoxydable avec de très petits trous, où seule l'huile sous pression passe, rejetant toutes les impuretés et les calcaires.
El procedimiento es el siguiente: <ul><li>Seccionar por el centro el filtro desechado. </li><li>Retirar el cartón filtrante y soldar en su lugar, una malla. Para realizar esta operación es necesario la­var la malla previamente con acido sulfúrico. La soldadura se realiza alrededor de la superficie de las tapas superior e inferior, que­dando de forma cilíndrica. </li><li>Soldar las dos tapas exteriores del filtro. </li></ul>|La procédure est la suivante: <ul><li>Sectionner le filtre mis au rebut par le centre. </li><li>Retirer le filtre en carton et souder à sa place, un maillage. Pour effectuer cette opération, il est nécessaire de laver le maillage au préalable avec de l'acide sulfurique. Le soudage est effectué autour de la surface des capots supérieur et inférieur, de manière cylindrique. </li><li>Souder les deux couvercles extérieurs du filtre.</li></ul>|
Jatibonico. Sancti Spiritus.|Jatibonico. Sancti Spiritus.|