# Adobe logement et toiture boutons - 187 / 2

> Traduction par Delphine

| Vivienda de adobe y techo de cogollo | Adobe logement et toiture cogollo |
| --- | --- |
| Esta casa de adobe de 4 X 6 m, cuenta con sala, cuarto, co­cina y pasillo, para su construccion se utiIizaron 6 horcones de madera rolliza, 30 parales, 36 cujes, 2 carretas de hierba, 12 metros de tierra blanca ca­liza (cocoa). En el techo se utilizaron 32 alfardas rollizas y 22 cujes, asi como 10 mil a 12 mil cogollos de cana. En el pi­so se utilizo 5 M3 de relleno, 250 racillas de barro, ademas se emplearon 3 sacos de cemen­to romano, para el asiento de las losas o racillas de barro y un saco de cemento P-250 para el derretido o cierre de la union de las losas, luego para buscar consistencia en las paredes, se le dio un resano. | Cette maison en pisé de 4 x 6 m, dispose d'un salon, d'une chambre à coucher, d'une cuisine et d'un couloir. Pour sa construction, 6 horcones de bois rond, 30 parales, ont été utilisées. 36 cujes, 2 chariots d'herbe, 12 mètres de calcaire blanc (cacao). Sur le toit, 32 rondins et 22 cujes ont été utilisés, ainsi que 10 000 à 12 000 boutons de canne. Sur le sol, 5 M3 de remplissage, 250 racillas d’argile ont été utilisés, ainsi que 3 sacs de ciment romain pour le siège des dalles ou racillas d’argile et un sac de ciment P-250 pour la fusion ou la fermeture du joint. des dalles, puis pour rechercher la cohérence dans les murs, une résona a été donnée. |
  ![fogon](/Traduction/img/Chapitre 8/187.pdf "187")
