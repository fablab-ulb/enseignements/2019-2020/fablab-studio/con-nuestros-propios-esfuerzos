# Maisons et matériaux de construction - 185 / 1

> Traduction par Delphine Siebehls

| Viviendas y materiales de construccion | Maisons et matériaux de construction  |
| --- | --- |
| Trata acerca del diseno, construccion y materiales utilizados en la edificacion de viviendas economicas. | Il traite de la conception, de la construction et des matériaux utilisés dans la construction de logements abordables. |