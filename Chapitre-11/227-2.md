# Tableau de démantèlement de la pompe d'injection à quatre emplois - 227 / 2

> traduit par : Abdellatif Siari

| Mesa para desarme de bomba de inyección con cuatro puestos de trabajo|Tableau de démantèlement de la pompe d'injection à quatre emplois|
| --- | --- |
|Los materiales que se emplean para su construcción son plancha metálica de chatarra, angulares de 2 pulgadas, tubos de 1,5 y 2 pulgadas. Con estos materiales se construye la mesa, que tiene situado en cada extremo un tornillo giratorio para el montaje y sujeción de la bomba de inyección, además de una banqueta giratoria, para que el estudiante efectúe la operación sentado.| Les matériaux utilisés pour sa construction sont de la ferraille métallique, des angles de 2 pouces, des tuyaux de 1,5 et 2 pouces. Avec ces matériaux, la table est construite avec une vis rotative à chaque extrémité pour le montage et la fixation de la pompe à injection, ainsi qu’un banc rotatif pour permettre à l’élève de s’asseoir.|
|San Cristóbal, Pinar del Río.| San Cristóbal, Pinar del Río.|