# Micropolygone pour l'irrigation et le drainage - 225 / 2

> traduit par : Florian Aliker

| Micropolígono para el riego y drenaje | Micropolygone pour l'irrigation et le drainage |
| --- | --- |
| En un area de 5 m de añcho por 7 de largo, dentro del area de la escuela, se construyó el sistema de riego y drenaje, que se debe de hacer en un cámpo sea tradicional o atipico y el alumno es capaz de asimilar en esta área de trabajo, todos los conocimientos necesarios que iba a captar en el campo, ya que la misma consta además, de una pequeña cisterna que abastece de agua al sistema completo llevado a maqueta. Esto contribuye al ahorro de combustible y cumple con el objetivo del proceso docente. | Le système d’irrigation et de drainage a été construit sur une surface de 5 m de long sur 7 m de large, dans la zone de l’école, il doit être fait dans un champ traditionnel ou atypique et l’élève doit être capable d'assimiler dans cette zone de travail toutes les connaissances nécessaires qui doivent être puisées sur le terrain, puisqu’il s’agit également, d’une petite citerne qui alimente en eau le système complet repris dans le modèle. Cela contribue aux économies de carburant et répond à l'objectif du processus d'enseignement. |
| Urbano Noris, Holguin | Urbano Noris, Holguin |
