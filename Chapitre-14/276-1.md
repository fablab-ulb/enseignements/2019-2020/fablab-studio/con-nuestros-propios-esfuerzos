# Troquel pour fabriquer des cristaux de montres - 276 /1
> Traduit par: Tchinda Julie

| Troquel para fabricar cristales de relojes | Troquel pour fabriquer des cristaux de montres |
| --- | --- |
| La maquina esta disenada con el objetivo de fabricar cristales de relojes. sobre todo en aquellos calibres que nunca han entrado a nuestro taller ejemplo:  2428 S.L. 2616 H.P.  2420 S.L 2229 W. 2214 W. 2628 H.R. Consta de un pedestal que esta formada por un tubo rectangular tipo cajuelas. con las siguientes dimensiones: Largo (125 m/m),  Ancho (112 m/m) 'Alto (51 m/m ) Espesor ( 4 m/m ). Posee dos tapas en su extremo 'de alumino. | La machine est conçue dans le but de fabriquer des cristaux de montres. particulièrement dans les calibres qui ne sont jamais entrés dans notre atelier, par exemple: 2428 S.L. 2616 h. 2420 S.L 2229 O. 2214 O. 2628 H.R. Il consiste en un piédestal formé par un réseau d'égouts de type tube rectangulaire. avec les dimensions suivantes: Longueur (125 m / m), largeur (112 m / m) 'Hauteur (51 m / m) Épaisseur (4 m / m). .Il a deux bouchons à son extrémité 'en aluminium.
Cumanayagua. CienCuegos. | Cumanayagua. Ciencuegos |
