# Méthodes et états à Majibacoa, Las Tunas - 268 / 2

> traduit par : Katia Mot

| Metodos y estilos en Majibacoa, Las Tunas | Méthodes et états à Majibacoa, Las Tunas |
| --- | --- |
| En el municipio Majibacoa se controla semanalmente el trabajo que realizan los organismos del Estado en interes del plan de produccion de articulos de alta demanda popular, la construccion de tuneles, la forestacion, el plan unico de vigilancia, el consumo de energia electrica, la limpieza y embellecimiento de las areas asignadas, el programa alimentario, el autoconsumo de los patios, solares yermos y en centros de trabajos y el uso indiscriminado de la fuerza de trabajo. | Dans la municipalité de Majibacoa, le travail effectué par les agences de l’État dans l’intérêt du plan de production pour les articles à forte demande, la construction de tunnels, le boisement, le plan de surveillance unique, la consommation d’énergie électrique, le nettoyage est surveillé chaque semaine et l’embellissement des zones assignées, le programme alimentaire, l’autoconsommation de chantiers, de terrains stériles et de centres de travail, ainsi que l’utilisation aveugle de la main-d’œuvre. | 