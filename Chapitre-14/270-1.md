# Modificateur de réducteur de panier d'épicerie de voyage - 270 / 1

> traduit par : Katia Mot 

| Modificator del reductor del carrito de la groa viajera | Modificateur de réducteur de panier d'épicerie de voyage | 
| --- | --- |
| Mediante la construccion de una base en el cacrito de la grua viajera se logro su funciohamiento horizontal, mejorando la lubricaci6n y facilitando el proceso de mantenimiento del trabajo. | Par la construction d'une base dans le registre du pont roulant son fonctionnement horizontal a été réalisé, améliorant la lubrification et facilitant le processus de maintenance des travaux. |
Taguasco, Sancli Spiritus