# Fabrication de chaussures pour boîtier de commande hydraulique - 265 / 2



> traduit par : Hélène Menu

| Fabricacion de zapatillas para caja de mando hidraulica | Fabrication de chaussures pour boîtier de commande hydraulique |
| --- | --- |
|Estas zapatillas se emplean en las cajas de mando de los equipos hidraulicos para evitar el derrache de lubricantes. Para su fabricacion es necessario construir un mandril de 2 pulgadas de diametro X2 pulgadas de largo, con sus respectivas medas interiores para lograr un molde eficaz. Este trabajo se logra por medio de la fundicion entre el material neopren y el molde, a traves del mandril. | Ces chaussures sont utilisées dans les boîtiers de commande des équipements hydrauliques pour éviter les débordements de lubrifiants. Pour sa fabrication, il est nécessaire de construire un mandrin de 2 pouces de diamètre X2pouces de long, avec leurs mesures intérieures respectives pour obtenir un moule efficace. Ce travail est obtenu par fusion entre le néoprène et le moule, à travers le mandrin.|
|Ademas se aprovechan los recortes de neopren y se evita el derroche de recursos. |De plus, des coupes en néoprène sont utilisées et le gaspillage de ressources est évité. |
|Los resultdos de las pruebas efectuadas han sido muy positivos. |Les résultats des tests effectués ont été très positifs. |
