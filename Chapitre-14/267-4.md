# Fan Star combiné riz - 267 / 4

> traduit par : Hélène Menu

| Estrella de ventilador de la combinada de arroz | Fan Star combiné riz|
| --- | --- |
| Partiendo de una barra deschada de 80 mm de diametro de ancho X 60 mm de largo :|A partir d'une barre dénudée de 80 mm de diamètre x 60 mm de long:
1, Tornelearla por ambos lados hasta llevarla a 50 mm. |1, le tourner des deux côtés jusqu'à ce qu'il soit 50 millimètres.
2,Dejar en el centro del eje un orificio en forma de buje de 30 mm de diametro.|2, laissez un trou de 30 mm de diamètre dans le centre de l’arbre.
3, Taladrar en el centro del eje un orificio en forma de buje 30 mm de diametro.| 3, percer un trou de 30 mm de diamètre dans le centre de l’arbre.
4, Conformar, partiendo de planchas desechadas de 6 , de espesor, 5 aletas con las siguientes dimensiones : 40 X 36 X 6.|4, formant, à partir des plaques jetées 6 ,, épais, 5 ailerons avec ce qui suitdimensions: 40 X 36 X 6.
5, Soldar las 5 aletas a uno de los extremos de la barra.|5, souder les 5 ailettes à une extrémité de la barre.
6, Taladrar en cada aleta un orificio de 8 mm de diametro.|6, percer un trou de 8 mm de diamètre dans chaque ailette.
7, Hacer en el diametro interior de la pieza, un cunero de 8 mm de porfundidad a todo lo largo de la pieza.|7, faites dans le diamètre intérieur de la pièce, un berceau de 8 mm de profondeur sur toute la longueur de la pièce. |