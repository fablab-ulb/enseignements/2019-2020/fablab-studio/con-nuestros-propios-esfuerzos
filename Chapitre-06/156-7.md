# Dissolvant - 156 / 7

> traduit par : Albert Almazov

| QUITAESMALTE | DISSOLVANT | 
| --- | --- |
| <ul></li> Ingredientes: <li></li> Alcohol de 90° <li></li> Brillo | <ul></li> Ingrédients: <li></li> alcool à 90° <li></li> Brillo ( à traduire ) |
| <ul></li> Preparación: <dd> Unir ambos ingredientes. | <ul></li> Préparation: <dd> Mélanger les ingrédients. |
| <ul></li> Aplicanión: <dd> Se recomienda no aplicarlo inmediato, sino, esperar 10 à 15 minutos después de preparado. Quita el esmalte sin dañar las uñas. El alcohol las fortalece. | <ul></li> Application: <dd> Il est recommandé de ne pas l'appliquer immédiatement, mais d'attendre 10 à 15 minutes après la préparation. Enlevez l'émail sans endommager les ongles. L'alcool les renforce. |
| Las Tunas | Las Tunas |
