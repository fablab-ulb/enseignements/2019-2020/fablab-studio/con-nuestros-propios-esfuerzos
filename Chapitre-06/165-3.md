# Poêle à bois - 165 / 3

> traduit par : Manon Dewilde 

| FOGÓN DE LEÑA | POELE A BOIS |
| --- | --- |
|Se construye con ladrillos refractarios, tola (plancha metálica) y un tubo para botar el humo. Se mantiene can buen calor todo el tiempo y no espande humo hacia los lados. Es muy económico ya que usa sólo la cuarta parte de la leña utilizada por otro tipo de fogón. Puede utilizarse dentro y fuera de locales.| Il est construit avec des briques réfractaires, de la tôle (plaque métallique) et un tube pour expulser la fumée. Il est maintenu à une bonne température tout le temps et ne répand pas de fumée sur les côtés. Il est très économique car il n'utilise qu'un quart du bois de chauffage utilisé par un autre type de poêle. Il peut être utilisé à l'intérieur et à l'extérieur.|
| Báguano, Holguin | Báguano, Holguin. |