# Encre rapide pour calzado - 162 / 5

> traduit par : Valentin Godeau

| Tinta rapida para calzado | Encre rapide pour calzado |
| --- | --- |
| Ingredientes: | Ingrédients: |
| -Alcohol: 1 litros |-Alcool: 1 litre  |
| -Cascara de mangle rojo: según el tono que se desee  |-Coquille de mangue rouge : selon le ton désiré  |
| -acido acético: 1g |-acide acétique: 1g  |
|Preparación:  | préparation: |
| Mezclar el mangle rojo y el alcohol. Remover y dejar en reposo durante 3 días. Anadir el acido acético, remover y envasar. |Mélanger la mangue rouge et alcool. Remuer et laisser reposer pendant 3 jours. Ajouter l'acide acétique, remuer et emballer.  |
| Rio Cauta, Granma |Rio Cauta, Granma  |

