# Eponge de yagua. - 161 / 3

> traduit par : Valentin Godeau

| Estropajo de yagua | Eponge de yagua. |
| --- | --- |
| Enterrar  o dejar  en  agua  la yagua  durante  7  u  .8  dias. Cuandc  este  jugi)sa  (babosa)  se lava  y  defleca.. Despues  se  hace el  estropajo que  debe  emplearse con  jabon  para  limpiarlas cazuelas | Enterrer ou laisser dans l'eau, Arrosez le pendant 7 ou 8 jours. Quand ce jus  (limace) est lave et defleca .. Ensuite c'est fait l'éponge à utiliser avec du savon pour les nettoyer les casseroles |
|La  Palma,  Pinar  del  Rio.  |La Palma, Pinar del Rio  |
