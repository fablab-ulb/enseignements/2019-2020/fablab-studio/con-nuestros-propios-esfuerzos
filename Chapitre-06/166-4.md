# Ecumoire - 166 / 4

> traduit par : Manon Dewilde 

| ESPUMADERA | ECUMOIRE  |
| --- | --- |
|Confeccionar una plantilla con las medidas propuestas en el esquema y los materiales a utilizarse son: zinc galvanizado, aluminio, remache (de aluminio o bronce), acero níquel o alambrón. | Faites un gabarit avec les mesures proposées dans le schéma et les matériaux à utiliser sont : zinc galvanisé, aluminium, rivet (aluminium ou bronze), acier au nickel ou fil machine. (voir img) |
| Bolivia, Ciego de Ávila. De manera similar la hacen en San Cristó, Pinar del Rio | Bolivie, Ciego de Ávila. De la même manière, ils le font à San Cristó, Pinar del Rio |

![espumadera](./Traduction/img/espumadera_166.4.png "166.4")