# Shampoing à l'aloès verra - 153 / 3

> traduit par : Jérémie Dalbet

| Champù de sábila  | Shampoing à l'aloès verra |
| --- | --- |
| <ul></li>Ingredientes: <li></li>Hojas de sábila .. 3 <li></li>Agua destillada o de lluvia .. 2<sup>1</sup><sub>2</sub> litros <li></li>Miel de abeja .. 2 tazas <li></li>Detergente .. 4 cucharadas <li></li>Vinagre .. 1 taza <li></li>Sal .. 2g | <ul></li>Ingrédients: <li></li>Feuilles d'Aloe Vera .. 3. <li></li>Eau distillée ou de pluie .. 2<sup>1</sup><sub>2</sub> litres <li></li>Miel d'abeilles .. 2 tasses. <li></li>Détergent .. 4 cuillérées. <li></li>Vinaigre .. 1 tasse. <li></li>Sel .. 2g. |
| Preparación: <ul></li> Pelar las hojas de sabila, picarlas en trocitos y ponerlas a hervir durante 30 minutos a fuego lento. Pasar por la batidora y colar. Anadir la miel, et detergente y la sal. Hervir durante 4 O 5 minutos, agregar el vinagre después de retirarlo del fuego. Se obtienen 1 136ml. | Préparation: <ul></li>Épluchez les feuilles d'aloès verra, coupez-les en petits morceaux et amenez-les à ébullition pendant 30 minutes à feu doux. Passer à travers le mélangeur et filtrer. Ajouter le miel, le détergent et le sel. Faites bouillir pendant 4 ou 5 minutes, ajoutez le vinaigre après l'avoir retiré du feu. 1 136 ml sont obtenus. |
|Cumanayagua, Clenfuegos |