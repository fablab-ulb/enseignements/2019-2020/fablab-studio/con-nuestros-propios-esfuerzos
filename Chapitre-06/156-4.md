# Eau de cologne au citron et mandarine pour après rasage - 156 / 4

> traduit par : Albert Almazov

| COLONIA DE LIMÓN Y MANDARINA PARA DESPUÉS DE AFEITARSE | EAU DE COLOGNE AU CITRON ET MANDARINE APRÈS LE RASAGE | 
| --- | --- |
| <ul></li> Ingredientes: <li></li> Alcohol absoluto .. 1 litro <li></li> Corteza de limon (seca) .. 250 g <li></li> Corteza de mandarina (seca) .. 100 g | <ul></li> Ingrédients: <li></li> Alcool absolu .. 1 litre <li></li> Pelures de citron (séchées) .. 250 g <li></li> Pelures de mandarine (séchées) .. 100 g |
| <ul></li> Preparación: <dd> Depositar todos los ingredientes en un frasco de color ámbar, de boca ancha; taparlo y dejar en reposo durante 15 días, agitándolo cada 3 o 4 días. Colarlo por un algodón y añadir igual proporción de agua destilada.| <ul></li> Préparation: <dd> Placer tous les ingrédients dans un pot ambré à large ouverture, couvrer et laisser reposer pendant 15 jours en remuant tous les 3 ou 4 jours. Filtrer à travers un tissu coton et ajouter la même quantité d'eau distillée. |
| Una variante de esta colonia consiste en utilizar pétalos de mariposa, jazmín, gardenia o rosas en cantidad de 150 g, en lugar de las cortezas de limón y mandarina. | Une variante de cette eau de cologne est possible en remplaçant les pelures de citron et mandarine par 150 g des pétales de ???papillons???, de jasmin, de gardénias ou de roses. |
| Ing. Rolando Gala Rodríguez Sandino, Pinar del Río | Ing. Rolando Gala Rodríguez Sandino, Pinar del Río |