# Peaux d'agrumes - 66/8

> traduit par : Decuyper Robin

| Hollejos de citricos | Peaux d'agrumes |
| --- | --- |
| Ingredientes : <ul><li> 25 libras de hollejos cocidos </li><li> 15 libras de azucar parda </li><li> 4 onzas de sal | Ingrédients: <ul><li> 25 livres de peaux cuites </li><li> 15 livres de sucre brun </li><li> 4 onces de sel |
| Pele y cocine el citrico, bote una o dos veces el agua de la coccion. Prepare un almibar agreguele los hollejos y la sal. Cocine hasta que cojan punto. Da 100 raciones de 4 onzas c/u. | Éplucher et cuire les agrumes, égoutter une ou deux fois l'eau de cuisson. Préparez un sirop ajoutez les peaux et le sel. Cuire jusqu'à ce qu'ils attrapent le point. Donnez 100 portions de 4 onces chacune. |
| Urbano Noris, Holguin.| Urbano Noris, Holguin. |
