# Rafraîchissement à base de pernambouc - 78 / 8
traduit par : El Morkadi Reda

|Refresco de palo Brasil| Rafraîchissement à base de pernambouc
| --- | --- |
Pelar el palo y extraerle el corazón antes de introducirlo en un recipiente con agua, donde debe permanecer 24 horas. Luego agregarle azúcar y colarlo. Con 5 libras del producto, 10 de azúcar y 25 litros de agua y ¼ litro de esencia obtendremos 100 refrescos. <br>Báguano, Holguín.| Pelez le pernambouc et extrayez le cœur avant de l'insérer dans un récipient rempli d'eau, où il doit rester 24 heures. Ajoutez ensuite le sucre et égouttez-le. Avec 5 livres de produit, 10 de sucre, 25 litres d'eau et ¼ de litre d'essence, nous aurons 100 sodas. <br> Báguano, Holguín.
