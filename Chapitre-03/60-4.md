##  Manioc frit avec saucisse - 60 / 4


> traduit par : Banga Fatima

| Frita de ruca con embutido | Manioc frit avec saucisse |
| --- | --- |
| <strong>Ingredientes :|<strong>Ingrédients :|
| <ul><li>750g de yuca <li>150g de embutido criollo <li>15g de harina de trigo <li> 15g de especias <li>110g de sal <li> 30g de huevos batidos <li>30g de grasa para grillar |<ul><li> 750g de manioc <li>150g de saucisse créole <li>15g de farine de blé <li> 15g d'épices <li>110g de sel <li> 30 g d'œufs battus <li>30 g de graisse à griller|
| Moler la vianda en la cuchilla fina, luego de haber sido hervida y mezclarla con el resto de los ingredientes. Formar las piezas y freirlas. | Broyer le vianda dans la lame mince, après qu'il ait été bouilli et le mélanger avec le reste des ingrédients. Formez les morceaux et faites-les frire. |
| Da 10 raciones de 32g c/u | Da 10 raciones de 32g c/u |
| Cienfuegos, De manera similar tambien las nacen en Las Tunas. | Cienfuegos, De la même manière ils naissent aussi à Las Tunas. |
