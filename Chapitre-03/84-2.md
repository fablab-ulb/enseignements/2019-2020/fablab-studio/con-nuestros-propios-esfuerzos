# Beurre de Tilapia - 84 / 2
> traduit par : Younes Cassandra

| Manteca de tilapia   | Beurre de Tilapia |
| --- | --- |
|De las tripas de la tilapia pueden extraerse unos filones blancos que al freírse expulsan grasa. Esta puede servir para cocinar el propio pescado.  |Dans les tripes du tilapia, on peut extraire des récifs blancs qui, une fois frits, chassent les graisses. Cela peut être utilisé pour cuire le poisson lui-même.  |
| Gibara, Holguín.  |  Gibara, Holguín. |||
