##  Croquettes d'aubergines aux petits pois - 58 / 5


> traduit par : Banga Fatima

| Croquetas de chicharos berenjena | Croquettes d'aubergines aux petits pois |
| --- | --- |
| <strong>Ingredientes :|<strong>Ingrédients :|
| <ul><li> 20 libras de berenjena <li>35 libras de harina de trigo <li>4 libras de manteca <li> 1 libra de cebolla <li>2 libras de sal <li>3 litros de salsa de tomate <li> 30 litros de caldo sustancioso. |<ul><li> 20 livres d'aubergines <li>35 livres de farine de blé <li>4 livres de beurre <li> 1 livre d'oignon <li>2 livres de sel <li>3 litres de sauce tomate <li> 30 litres de bouillon substantiel. |
| La forma de elaboracion es identica a la preparacion de la croqueta tradicional.| Le mode d'élaboration est identique à la préparation de la croquette traditionnelle. |
| Da 1000 raciones de 40 g c/u. |  Donne 1000 portions de 40 g chacune |
| Los Palacics, Pinar del Rio, De manera similar las hacen en Las Tunas. | Los Palacics, Pinar del Rio, Ils sont également fabriqués à Las Tunas. |
