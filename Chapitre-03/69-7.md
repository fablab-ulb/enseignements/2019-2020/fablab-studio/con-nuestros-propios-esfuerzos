# Confiture concentrée de concombre - 69/7

> traduit par : Decuyper Robin

| MERMELADA CONCENTRADA DE PEPINO | Confiture concentrée de concombre |
| --- | --- |
| Ingredientes : <ul><li> 114 kg de pulpa de pepino hervido </li><li> 92 kg de azucar refino </li><li> 100 g de acido citrico | Ingrédients: <ul><li> 114 kg de pulpe de concombre bouillie </li><li> 92 kg de sucre raffiné </li><li> 100 g d'acide citrique |
| Utilice pepino sin pelar; hiervalo hasta ablandar la cascara y que suelte la mancha. Luego paselo por el separador para eliminar las semillas y hagalo pulpa.  | Utilisez le concombre non pelé; faites-le bouillir jusqu'à ce que la coque se ramollisse et libère la tache. Ensuite, passez-le à travers le séparateur pour retirer les graines et en faire une pâte. |
| Unalo al resto de los ingredientes y cocine hasta que adquiera punto de mermelada concentrada. | Combinez-le avec le reste des ingrédients et faites cuire jusqu'à obtenir une confiture concentrée. |
| Baracoa, Guantanamo. Tambien en Baguano, Holguin. | Baracoa, Guantanamo. Toujours à Baguano, Holguin. |
