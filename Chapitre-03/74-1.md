# Confiture de riz à la saveur d'orange  - 74 / 1
traduit par : El Morkadi Reda

|Dulce de arroz con sabor a naranja |Confiture de riz à la saveur d'orange
| --- | --- |
Ingredientes:<li>3 libras de arroz<li>2 litros de jugo de naranja<li>8 libras de azúcar<li>1 onza de azúcar<li>1 onza de sal<li>100 gramos de canela en polvo<br>Cocine el arroz con el agua y la sal a fuego lento hasta que se ablande. Añada entonces el jugo de naranja y el azúcar y deje hervir revolviendo constantemente. Sírvalo con canela en polvo por encima.<br>Minas de Matahambre, Pinar del Rio. | Ingrédients:<li>3 livres de riz<li>2 litres de jus d'orange<li>8 livres de sucre<li>1 once de sucre<li>1 once de sel<li>100 grammes de cannelle en poudre<br>Cuire le riz avec l'eau et le sel à feu doux jusqu'à ce qu'il se ramollisse. Ajoutez ensuite le jus d'orange et le sucre et laissez mijoter constamment. Servir avec de la cannelle en poudre sur le dessus.<br>Minas de Matahambre, Pinar del Rio.
