# Rafraîchissement à base de fenouil - 79 / 4
traduit par : El Morkadi Reda

|Refresco de hinojo| Rafraîchissement à base de fenouil
| --- | --- |
Hervir 30 libras de yerbas de hinojo hasta obtener un extracto. Agregarle después 12 libras de azúcar y 23 litros de agua. Da 100 raciones.<br>Báguano, Holguín.|Faites bouillir 30 livres d'herbes de fenouil jusqu'à ce que vous obteniez un extrait. Ajoutez ensuite 12 livres de sucre et 23 litres d’eau. Donnez 100 portions. <br> Báguano, Holguín.
