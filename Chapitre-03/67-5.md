# Melon Doux II - 67/5

> traduit par : Decuyper Robin

|Dulce de melon II  | Melon Doux II |
| --- | --- |
| INgredientes : <li> melon <li> azucar <li> canela, anis, cascara de limon (al gusto)  | Ingrédients: <li> melon <li> sucre <li> cannelle, anis, zeste de citron (au goût) |
| Quite  el melon la cascara y la para ..ugosa que sirve parahacer jugo refresco. El resto de la fruta piqueloen cuadritos como la fruta bomba y cocinelo en agua suficie,te pura ablanbarlo un poco; Agregue el resto le los ingredientes y continue a coccion por 15 o 20 minutos. | Retirez le melon, épluchez-le et utilisez-le pour faire du jus de soda. Le reste du fruit piqueloen dans des carrés comme la bombe de fruits et cuire dans l'eau de surface, vous pur adoucissez un peu; Ajouter le reste des ingrédients et poursuivre la cuisson 15 à 20 minutes. |
| Baguano, Holguin | Baguano, Holguin |
