# Nougat à la papaye  - 73 / 8
traduit par : El Morkadi Reda

|Turrón de fruta bomba |Nougat à la papaye
| --- | --- |
Ingredientes:<li>2 lb. De fruta bomba hervida sin cascara<li>2 lb. De almíbar hecha con azúcar<br>Muela la fruta bomba después de escurrirla bien. Agregue el almíbar y cocine hasta que coja el punto deseado. Da 20 raciones.<br>Báguano, Holguín. | Ingrédients:<li>2 livres de papaye bouillie sans coquille. <li>2 livres de sirop à base de sucre<br>Broyez la papaye après l'avoir bien drainée. Ajouter le sirop et cuire jusqu'à ce que vous preniez le point désiré. Donne 20 portions. <br>Báguano, Holguín.
 
