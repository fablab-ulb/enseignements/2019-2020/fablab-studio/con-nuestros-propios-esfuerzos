# Saoco doux (cocktail de Cuba) - 81 / 8
traduit par : El Morkadi Reda

|Saoco dulce| Saoco doux (cocktail de Cuba)
| --- | --- |
Mezclar 148 litros de agua de coco, 92 kg de azúcar y 96 litros de alcohol. Mezclarlos y ponerlos en reposo.<br>Baracoa, Guantánamo | Mélangez 148 litres d’eau de coco, 92 kg de sucre et 96 litres d’alcool. Mélangez-les et mettez-les au repos. <br> Baracoa, Guantanamo
