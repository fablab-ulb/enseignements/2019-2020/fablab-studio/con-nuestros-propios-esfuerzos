##  Pois frits - 60 / 3


> traduit par : Banga Fatima

| Frita de chicharos | Pois frits |
| --- | --- |
| <strong>Ingredientes :|<strong>Ingrédients :|
| <ul><li>20 libras de chicharos <li> 20 libras de harina <li>7 libras de grasa <li> 1 pomo de puré de tomate sal a gusto <li>sofrito a gusto.| <ul><li> 20 livres de petits pois <li>20 livres de farine <li>7 livres de graisse <li>1 purée de tomates en purée sel d'arçons au goût <li>sofrito au goût.
| <ul><li>Dejar los chicharos en remojo de un dia para otro, luego ablandarlos y botarle el agua para pasarlos por la licuadora. <li>En una cazuela aparte, preparar un sofrito y agregarselo a la masa. Vertirle 10 litros de agua (puede ser la misma donde se cocinaron). Cuando este hirviendo anadirle la harina e ir batiendo continuamente con una paleta hasta conformar la masa. Moldear las fritas para freir u hornear. | <ul><li>Laisser tremper les petits pois toute la nuit, puis les ramollir et jeter l'eau pour les faire passer dans le mélangeur. <li>Dans une casserole séparée, préparer un sofrito et l'ajouter à la pâte. Versez 10 litres d'eau (peut être la même que celle où ils ont été cuits). Lorsqu'il est en ébullition, ajouter la farine et battre continuellement avec une palette jusqu'à ce que la pâte soit formée. Mouler les frites pour les faire frire ou cuire au four. |
| Da 1200 raciones de 29g c/u. | --- |
| San Cristobal, Pinar del Rio, de manera similar tambien las hacen en Las Tunas. | Donne 1200 rations de 29g chacune. |
