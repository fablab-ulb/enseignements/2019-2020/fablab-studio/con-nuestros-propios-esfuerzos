##  Sandwichs de pâtes aux bananes et au poisson - 56 / 2


> traduit par : Banga Fatima

| Pasta de bocaditos con platano y pescado | Sandwichs de pâtes aux bananes et au poisson |
| --- | --- |
| <strong>Ingredientes :|<strong>Ingrédients :|
| <ul><li>958g de platano <li>958g de pescado <li> 208g de puré de tomate <li>83g de salsa criolla <li> 41g de sal <li> 1916g de salsa bechamel <li>4000g de pan |<ul><li>958g de banane<li>958 g de poisson<li>208g de purée de tomates<li>83g de sauce créole<li>41 g de sel<li>1916g de sauce béchamel<li>4000g de pain|
| <ul><li>Hervir los platanos hasta que esten blandos y cocer el pescado, despues de molerlos con la cuchilla fina y agregarle la salsa bechamel junto a las sazones. <li>Mezclarlo todo hasta obtener una pasta homogenea.| <ul><li>Faire bouillir les bananes jusqu'à ce qu'elles soient tendres et cuire le poisson, après les avoir broyées avec la lame fine et ajouter la sauce béchamel avec les assaisonnements. <li>Mélanger le tout jusqu'à l'obtention d'une pâte homogène.|
| Da 100 raciones cada una con 80 gramos | Da 100 raciones cada una con 80 gramos |
| Lagas, Cienfuegos. |
