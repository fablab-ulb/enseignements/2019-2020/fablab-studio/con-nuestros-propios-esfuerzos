#  Hachis de boeuf avec légumes - 55 / 2

> traduit par : Banga Fatima

| Picadillo de ternera con vegetales | Hachis de boeuf avec légumes  |
| --- | --- |
| <strong>Ingredientes :|<strong>Ingrédients :|
|<ul><li>900g de carne de res<li>850g de zanahoria o remolacha<li>500g de salsa criolla<li>30g de sal<li>1g de pimienta<li>30g de limón<li>50g de vino seco<li>30g de aceite vegetal<li>700g de caldo sustancioso|<ul><li>900g de boeuf<li>850 g de carottes ou de betteraves<li>500g de sauce créole<li>30g de sel<li>1 g de poivre<li>30 g de citron<li>50g de vin sec<li>30g d'huile végétale<li>700 g de bouillon substantiel|
|Moler la carne con el disco fino y luego ablandar los vege¬tales, dejándolos en la misma cocción. Elaborar la salsa criolla y el caldo sustancioso. Lue¬go saltear la carne y agregarle la salsa criolla, el caldo sustan¬cioso y, los vegetales. Añadirle el jugo de limón y el vino seco para después puntearlo con sal y pimienta. Cocinar fue¬go lento hasta que consuma los líquidos.|Broyer la viande en fine rondelle, puis faire revenir les légumes, en les laissant dans la même casserole. Préparer la sauce créole et le bouillon substantiel. Faire revenir la viande et ajouter la sauce créole, le bouillon substantiel et les légumes. Ajouter le jus de citron et le vin sec et piquer avec du sel et du poivre. La cuisson était lente jusqu'à ce qu'il consomme les liquides.
|Cumanayagua, Cienfuegos.|
