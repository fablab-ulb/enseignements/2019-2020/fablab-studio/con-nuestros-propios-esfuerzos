# Râpe de noix de coco  - 86 / 1
> traduit par : Younes Cassandra

| Ralladora de coco  | Râpe de noix de coco |
| --- | --- |
|Construida a partir de materiales de desecho recuperables (acero y bronce) de recortería. La máquina está compuesta por varias piezas parecidas a las de moledoras de carne. Su altura es de 400 mm.<br> Después de confeccionar todas sus piezas, seis en total, se ensamblan mediante soldadura.  | Construit à partir de déchets récupérables (acier et bronze) provenant de découpes. La machine est composée de plusieurs pièces similaires à celles des hachoirs à viande. Sa hauteur est de 400 mm.  <br>Après avoir fabriqué toutes leurs pièces, six au total, elles sont assemblées par soudage.   |
|  Pinar del Rio, Pinar del Rio.  | Pinar del Rio, Pinar del Rio.  |||