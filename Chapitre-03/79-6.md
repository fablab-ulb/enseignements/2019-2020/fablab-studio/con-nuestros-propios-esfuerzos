# Jugo de zanahoria y plátano - 79 / 6
traduit par : El Morkadi Reda

|Jugo de zanahoria y plátano| Jus de carotte et de banane
| --- | --- |
Ingredientes:<li>734g de zanahoria<li>587g de mermelada de plátano<li>1028g de agua<li>2g de sal<br>Rallar y batir las zanahorias, luego colarla y ligarla bien con el resto de los componentes. Guardarlo en recipiente de cristal. Da 10 raciones de 150g cada una.<br>Cumanayagua, Cienfuegos.| Ingrédients: <li> 734g de carotte <li> 587g de confiture de bananes <li> 1028g d'eau <li> 2g de sel <br> Râpez et battez les carottes, puis égouttez-les et serrez bien avec le reste des composants. Rangez-le dans un récipient en verre. Donner 10 portions de 150g chacune. <br> Cumanayagua, Cienfuegos.
