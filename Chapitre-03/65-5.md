# Bâton de banane - 65/5

> traduit par : Decuyper Robin

| Platano en barra | Bâton de banane |
| --- | --- |
| Ingredientes: <ul><li> 1620 g de platano maduro con cascara </li><li> 1120 g de azucar </li><li> 1 g de sal </li><li> 1 g de jugo de limon | Ingrédients: <ul><li> 1620 g de banane mûre avec pelure </li><li> 1120 g de sucre </li><li> 1 g de sel </li><li> 1 g de jus de citron |
| Hervir el platano. Ya blando, batalo hasta obtener una pulpa. Agregue azucar un poco e agua de la coccion y el ju de limon. Ponga al fuego de nuevo y cuando espese, vierta en un molde. | Faire bouillir la banane. Maintenant, doucement, battez jusqu'à ce que vous obteniez une pulpe. Ajoutez un peu de sucre, de l'eau de cuisson et du jus de citron. Remettez le feu et quand il épaissit, versez dans un moule. |
| Abreus, Cienfuegos| Abreus, Cienfuegos |
