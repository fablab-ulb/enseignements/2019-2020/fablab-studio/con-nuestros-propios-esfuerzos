##  Pâté mixte - 55 / 7


> traduit par : Banga Fatima

| Mixta Lajera | Pâté mixte |
| --- | --- |
| <strong>Ingredientes :|<strong>Ingrédients :|
| <ul><li>17020g de tocineta<li>11500g de plátanos<li>9660g de embutidos<li>460g de salsa criolla<li>580g de sal<li>1000g de grasa<li>104g de agua<li>11500g de coles frescas|<ul><li>17020g de bacon<li>11500g de bananes<li>9660g de saucisses<li>460g de sauce créole<li>580 g de sel<li>1000g de matières grasses<li>104g d'eau<li>11500g de choux frais
| Cortar las carnes en cuadros, luego hervir las coles y los plátanos para seccionarlas, también de la misma forma. Después debe prepararse la salsa criolla, vertiéndola en un recipiente adecuado donde podrán ser adicionados, primero los vegetales y las viandas, más tarde las carnes. Dejarlo cocinar. |Couper la viande en carrés, puis faire bouillir les choux et les bananes pour les couper, de la même manière. Ensuite, la sauce créole doit être préparée, en la versant dans un récipient approprié où les légumes et les viandas peuvent être ajoutés en premier, puis les viandes. Laissez-le cuire.
| Da 100 raciones de 348g c/u.| Cette recette donne 100 portions de 348g chacune. |
Lajas, Cienfuegos.