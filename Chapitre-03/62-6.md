# Pain d'igname - 62/6

> traduit par : Decuyper Robin

| Pan de ñame  | Pain d'igname |
| --- | --- |
| Rallar el name y agregarle azuca, especias, dulces y leche de coco. Ligar bien todos los componentes y vaciar la mezcla en el recipiente que tiene las paredes cubiertas con hojas de platanos, para cocinarle se le ponen brazas a la tapa (horneado). Este pan puede durar hasta tres dias a temperatura ambiente. |Râpez le nom et ajoutez le sucre, les épices, les bonbons et le lait de coco. Liez bien tous les composants et videz le mélange dans le récipient dont les parois sont recouvertes de feuilles de bananier. Pour le faire cuire, placez-le sur le couvercle (cuit). Ce pain peut durer jusqu'à trois jours à la température ambiante. |
| Maisi, Guantanamo |  Maisi, Guantanamo |
