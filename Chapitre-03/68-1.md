# Meringues dures - 68 / 1

> traduit par : Decuyper Robin

| Merenguitos duros | Meringues dures  |
| --- | --- |
| Ingredientes : <ul><li> 120 hueos </li><li> 8 libras de azucar | Ingrédients : <ul><li> 120 oeufs </li><li> 8 livres de sucre |
| Separe claras y yemas. Licue las claras con el azucar y pongalas en el horno, en pequenas porciones, durante 15 rninutoo. Da 1870 unidades. | Séparez les blancs et les jaunes. Mélangez les blancs d'œufs avec le sucre et mettez-les au four, par petites portions, pendant 15 minutes. Il donne 1870 unités. |
| La Palma, Pinar del Rio | La Palma, Pinar del Rio  |
