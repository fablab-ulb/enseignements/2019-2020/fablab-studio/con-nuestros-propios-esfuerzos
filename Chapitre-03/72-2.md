# Natillas naturelle (crème aux oeufs espagnoles) - 72 / 2
traduit par : El Morkadi Reda

|Natilla natural |Natillas naturelle (crème aux oeufs espagnoles)
| --- | --- |
Ingredientes: <li> 5 ¼  libras de harina<li>7 ½  libras de azúcar refino<br>Haga un almíbar con el azúcar, cuando esté hirviendo agregue la harina y algún saborizante a gusto. Remueva hasta que espese. Da 100 raciones.<br>Báguano, Holguín.| Ingrédients: <li>5¼ livres de farine<li>7½ livres de sucre raffiné<br>Faites un sirop avec le sucre, quand il bout, ajoutez la farine et un peu d’arôme. Remuer jusqu'à épaississement. Donne 100 portions.<br>Báguano, Holguín
