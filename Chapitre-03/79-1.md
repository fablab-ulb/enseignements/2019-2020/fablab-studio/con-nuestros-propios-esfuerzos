# Rafraîchissement à base de cassier - 79 / 1
traduit par : El Morkadi Reda

|Refresco de caña fístula | Rafraîchissement à base de cassier
| --- | --- |
Pelar la caña fístula y extraerle las semillas. Preparar un almíbar claro y mezclarlo todo en la batidora. Con 8 litros de pulpa, 20 de agua y 12 libras de azúcar, obtendremos 100 refrescos<br>Báguano, Holguín. | Pelez le cassier et extrayez les graines. Préparez un sirop transparent et mélangez le tout dans le mélangeur. Avec 8 litres de pâte, 20 d'eau et 12 livres de sucre, nous aurons 100 boissons non alcoolisées<br> Báguano, Holguín.
