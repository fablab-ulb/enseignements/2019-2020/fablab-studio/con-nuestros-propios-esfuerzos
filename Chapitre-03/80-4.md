# Vin de feuille de corossol - 80 / 4
traduit par : El Morkadi Reda

|Vino de hojas de guanábana| Vin de feuille de corossol
| --- | --- |
Ingredientes:<li>60 libras de azúcar<li>80 litros de agua<li>4g de sal<li>25g de levadura<li>Un cubo de hojas de guanábanas desmenuzadas<br>El tanque donde se vaya a elaborar el vino, debe de estar a 50cm de altura sobre un parle para lograr una mejor decantación. Luego de verter los componentes, taparlos con tela fina y dejarlos reposar durante 28 ó 30 días. Extraer el vino con una manguera.<br>Se obtienen 90 litros.<br>Sagua de Tánamo, Holguín.| Ingrédients: <li> 60 livres de sucre <li> 80 litres d’eau <li> 4g de sel <li> 25g de levure <li> Un seau de feuilles de corossol râpées <br> La cuve où le vin doit être fabriqué , doit avoir une hauteur de 50cm sur un parle pour obtenir une meilleure décantation. Après avoir versé les composants, couvrez-les avec un chiffon fin et laissez-les reposer 28 ou 30 jours. Extraire le vin avec un tuyau.<br>On obtient 90 litres. <br> Sagua de Tánamo, Holguín.
