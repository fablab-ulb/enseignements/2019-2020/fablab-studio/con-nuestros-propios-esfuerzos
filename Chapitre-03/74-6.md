# Confiture de concombres  - 74 / 6
traduit par : El Morkadi Reda

|Dulce de plátano madura | Confiture de concombres
| --- | --- |
Ingredientes:<li>333 libras de pepino pelado<li>166 libras de azúcar<li>58g de canela<br>Pique en dos los pepinos y extraiga las semillas. Cocínelos hasta que el agua comience a hervir. Escúrralos y añádales el azúcar y la canela. Mantenga al fuego hasta que tome punto de almíbar.<br>Los Palacios, Pinar del Rio<br>De forma similar, pero utilizando pepino rallado en:<br>Rodas, Cienfuegos.| Ingrédients:<li>333 livres de concombre pelé<li>166 livres de sucre<li>58g de cannelle<br>Couper les concombres en deux et extraire les graines. Faites-les cuire jusqu'à ce que l'eau commence à bouillir. Égoutter et ajouter le sucre et la cannelle. Garder le feu jusqu'à la pointe du sirop.<br>Les palais, Pinar del Rio<br>De même, mais en utilisant du concombre râpé dans:<br>Rodas, Cienfuegos.
