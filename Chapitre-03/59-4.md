##  Tamales de bananes (bacan) - 59 / 4


> traduit par : Banga Fatima

| Tamales de platano (bacan)| Tamales de bananes (bacan) |
| --- | --- |
| Rayar el platano fruta y adicionarle un sofrito con pedacitos de carne. Pasar las hojas verdes del platano por fuego para ponerlas suave. Envolver la masa en las hojas, atarla con la cepa de la misma planta y ponerlo a hervir durante una hora. | Gratter la banane et ajouter un sofrito avec des morceaux de viande. Chauffer les feuilles vertes de la banane pour les ramollir. Envelopper la pâte dans les feuilles, l'attacher avec la passoire de la même plante et la faire bouillir pendant une heure.|
|  Maisi, Guantanamo.|
