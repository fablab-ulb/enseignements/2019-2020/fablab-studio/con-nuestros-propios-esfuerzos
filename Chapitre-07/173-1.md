# Base ou gréement avec terre (boue) - 173 / 1

> Traduit par: Diego Río

|Base o aparejo con tierra (barro).|Base ou gréement avec terre (boue).|
|---|---|
|Materiales:|Matériaux:|
|1 galón de tierra (barro amarillo o rojo).|1 gallon de terre (boue jaune ou rouge).|
|Galón y medio de agua.|Gallon et demi d'eau.|
|Mezclar las proporciones mencionadas y luego colar 3 o 4 veces para extraer los terrones de tierra, piedrecitas y perdigones que pudiera tener. Este mezclado queda en forma de pasta y puede permanecer guardado el tiempo que se desee.|Mélangez les proportions mentionnées, puis passez 3 ou 4 fois pour extraire les morceaux de terre, les cailloux et les granulés que vous pourriez avoir. Ce mélange est sous forme de pâte et peut être stocké aussi longtemps que vous le souhaitez.|
|De secarse se le adicionara un poquito de agua.|Si séché, un peu d'eau sera ajouté.|
|Se resuelve el preparado anterior y se toman dos terceras partes de un galón adicionándosele una tercera parte de galón de cola aguada acabada de hacer. Después de revolver queda lista para aplicar al mueble, ya sea de cartón, tabla o madera. Se deben dar dos manos.|La préparation précédente est résolue et deux tiers de gallon sont pris en ajoutant un tiers de gallon de queue liquide. Après agitation, il est prêt à être appliqué sur les meubles, qu’ils soient en carton, en carton ou en bois. Deux mains doivent être données.|
|Esta base o aparejo después de preparada debe darse en el día y no debe ser utilizada en muebles en sentarse.|Cette base ou installation après la préparation doit être donnée dans la journée et ne doit pas être utilisée dans les meubles en position assise.|
|Rodas, Cienfuegos.|