# Stylo à eau en plastique - 170/2

> traduit par : Decuyper Robin

| Pluma de agua plàstica | Stylo à eau en plastique |
| --- | --- |
| Esta pluma de agua ha sufrido algunos cambios con respecto a la original, superando su calidad. Proceso de elaboración: Primero se seleccioña la materia prima y le extraemos su impureza. Después de triturada, se pasa a la máquina. la cual en forma de inyección imprime el plástico en el molde, mediante una fuerza hidráulica de 250 atmósfera. La pluma está compuesta por 5 partes, las cuales se fabrican siguiendo el mismo principio. La terminación se realiza a mano, recortando los bordes y puntos de inyección con una chaveta, y posteriormente se arma  | Ce stylo à eau a subi quelques modifications par rapport à l'original, dépassant sa qualité. Processus de préparation: La matière première est d'abord sélectionnée et son impureté est extraite. Après écrasement, il est passé à la machine, qui sous la forme d'une injection imprime le plastique dans le moule, en utilisant une force hydraulique de 250 atmosphères. Le stylo est composé de 5 pièces, qui sont fabriquées selon le même principe. La terminaison est faite à la main, en coupant les bords et les points d’injection avec une clé, puis en assemblant |
| Fomento, Sancti Spiritus | Fomento, Sancti Spiritus  |
