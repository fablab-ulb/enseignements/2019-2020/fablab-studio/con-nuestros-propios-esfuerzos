# Joints d'autocuiseurs - 183 / 3

> traduit par : Florian Aliker

| Juntas de olla de presión | Joints d'autocuiseurs |
| --- | --- |
| Juntas para olla de presión.<br>Se fabrican mediante un molde maquinado, el cual se monta en una prensa que utiliza como fuente de energía eléctrica resistencias.<br>Como materia prima emplea XG-24 (goma resistente al calor, agua, aceite y otras sustancias químicas). Con 1 kg de esta materia prima se hacen alrededor de 20 juntas de olla de presión. | Joints pour autocuiseur.<br>Ils sont fabriqués à l'aide d'un moule usiné, monté sur une presse utilisant des résistances comme source d'énergie électrique.<br>En tant que matière première, il utilise le XG-24 (caoutchouc résistant à la chaleur, eau, huile et autres substances chimiques). Avec 1 kg de cette matière première, environ 20 joints d’autocuiseur sont fabriqués. |
| Aguada de Pasajeros, Cienfuegos | Aguada de Pasajeros, Cienfuegos |