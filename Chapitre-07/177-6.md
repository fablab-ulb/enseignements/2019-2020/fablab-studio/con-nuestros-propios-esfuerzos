# Retour en arrière la télévision des Caraïbes - 177 / 6

> traduit par : Julien Zabala

| Fly back el Tv Caribe |Retour en arrière la télévision des Caraïbes|
| --- | --- |
| Se toma fibra aislante y se hace un cono de igual diámetro al original. Como primera operación hacemos una capa de 45 vueltas con alambre No. 26, seguidamente con alambre No. 33, 2 capas de 10 vueltas y después con alambre No. 35 hacemos 25 capas de 65 vueltas. Entre capa. Y capa empleamos como aislante el nylon.|La fibre isolante est prise et un cône de diamètre égal est fait à l'original. Lors de la première opération, nous réalisons une couche de 45 tours avec le fil n ° 26, puis avec le fil n ° 33, 2 couches de 10 tours, puis avec le fil n ° 35, nous réalisons 25 couches de 65 tours. Entre couche Et nous utilisons du nylon comme isolant.
|Todos los municipios. Cienfuegos. Similar trabajo se realiza en el municipio Pinar del Rio, capital de la misma provincia.|Todos los municipios. Cienfuegos. Similar trabajo se realiza en el municipio Pinar del Rio, capital de la misma provincia. |